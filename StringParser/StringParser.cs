﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringParser
{
    public static class StringParser
    {
        public static int ParseToInt(string parseString)
        {
            if (String.IsNullOrWhiteSpace(parseString))
            {
                throw new ArgumentNullException();
            }

            byte[] asciiBytes = Encoding.ASCII.GetBytes(parseString);

            CheckArrayOfBytes(asciiBytes);

            int returnIntValue = 0;
            Array.Reverse(asciiBytes);
            int counter = 1;
            asciiBytes.ToList().ForEach((x) =>
            {
                returnIntValue += (Convert.ToChar(x) - 48) * counter;
                counter *= 10;
            });

            return returnIntValue;
        }

        private static bool CheckArrayOfBytes(byte[] bytesArray)
        {
            for (int i = 0; i < bytesArray.Length; i++)
            {
                if ((bytesArray[i] >= 48 && bytesArray[i] <= 57) || (bytesArray[i] == 45))
                {
                    if (bytesArray[i] == 45 && i != 0)
                    {
                        throw new FormatException();
                    }
                }
                else
                {
                    throw new FormatException();
                }
            }

            return true;
        }
    }
}
