﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringHandler
{
    public static class StringReader
    {
        public static string GetFirstLetter(string enterString)
        {
            if (String.IsNullOrWhiteSpace(enterString))
            {
                throw new ArgumentException();
            }

            return enterString[0].ToString();
        }
    }
}
