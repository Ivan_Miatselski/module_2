﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                string value = Console.ReadLine();
                try
                {
                    Console.WriteLine(StringReader.GetFirstLetter(value));
                }
                catch
                {
                    Console.WriteLine("Input string not should to be empty or null!");
                }
            }
        }
    }
}
